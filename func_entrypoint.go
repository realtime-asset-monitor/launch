// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package launch

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/cev"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// EntryPoint function entry point.
func EntryPoint(ctxEvent context.Context, cloudEvent cloudevents.Event) error {
	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv
	ev.Step = glo.Step{
		StepID:        fmt.Sprintf("%s/%s", cloudEvent.Context.GetSource(), cloudEvent.Context.GetID()),
		StepTimestamp: cloudEvent.Context.GetTime(),
	}
	now := time.Now()
	d := now.Sub(ev.Step.StepTimestamp)

	b, _ := json.MarshalIndent(cloudEvent, "", "  ")
	glo.LogStartCloudEvent(ev, b, d.Seconds(), &now)

	pubsubData := &cai.WrappedPubSub{}
	if err := cloudEvent.DataAs(pubsubData); err != nil {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("cloudEvent.DataAs(pubsubData) %v", err), "", "")
		return nil
	}

	triggerName := string(pubsubData.Message.Data)
	cacheAge := now.Sub(global.actionsRepo.Version)
	if global.serviceEnv.CacheMaxAgeMinutes == 0 || cacheAge.Minutes() > global.serviceEnv.CacheMaxAgeMinutes {
		var err error
		global.actionsRepo, err = refreshCache(ctxEvent, global.actionsRepoBucket,
			global.serviceEnv.LogOnlySeveritylevels,
			global.serviceEnv.Environment,
			microserviceName)
		if err != nil {
			glo.LogCriticalRetry(ev, fmt.Sprintf("refreshCache %v", err), "", "")
			return err
		}
		a, err := json.MarshalIndent(global.actionsRepo, "", "   ")
		if err != nil {
			glo.LogWarning(ev, fmt.Sprintf("json.MarshalIndent(global.actionsRepo %v", err), "")
		}
		glo.LogInfo(ev, fmt.Sprintf("actions cache updated, trigger %s", triggerName), string(a))
	}
	actions, ok := global.actionsRepo.TriggerActions[triggerName]
	if !ok {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("triggerName %s not found in actions repo", triggerName), "", "")
		return nil
	}
	var publishedCount int
	ev.StepStack = []glo.Step{ev.Step}
	for _, action := range actions {
		action := action // prevent G601
		action.StepStack = ev.StepStack
		_, err := cev.Publish(ctxEvent,
			microserviceName,
			action.Kind,
			&action,
			global.actionClient,
			global.serviceEnv.ProjectID,
			global.env.KRevision)
		glo.LogInfo(ev, action.Kind, string(action.Metadata))
		if err != nil {
			glo.LogCriticalNoRetry(ev, fmt.Sprintf("cev.Publish %v", err), "", "")
		} else {
			publishedCount++
		}
	}
	glo.LogFinish(ev, triggerName, fmt.Sprintf("published %d/%d actions", publishedCount, len(actions)), time.Now(), "scheduled", "", "", "", 0)
	return nil
}
