// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package launch

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/storage"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
	"google.golang.org/api/iterator"
)

func TestIntegEntryPoint(t *testing.T) {
	testCases := []struct {
		name            string
		trigger         string
		folderPath      string
		wantMsgContains []string
	}{
		{
			name:       "full",
			trigger:    "at_00am00_on_day_of_month_1_in_january",
			folderPath: "testdata/action01",
			wantMsgContains: []string{
				"start cloudevent",
				"finish at_00am00_on_day_of_month_1_in_january",
				"published 7/7 actions",
			},
		},
	}
	projectID := os.Getenv("LAUNCH_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var LAUNCH_PROJECT_ID")
	}
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatalf("storage.NewClient %v", err)
	}
	actionsRepoBucketName := projectID + "-actionsrepo"
	actionsRepoBucket := storageClient.Bucket(actionsRepoBucketName)

	now := time.Now()
	var cloudEvent cloudevents.Event
	cloudEvent.SetSpecVersion("1.0")
	cloudEvent.SetID("3912279448406699")
	cloudEvent.SetSource("//pubsub.googleapis.com/projects/qwerty-ram-qa-100/topics/actionTrigger")
	cloudEvent.SetType("google.cloud.pubsub.topic.v1.messagePublished")
	cloudEvent.SetDataContentType("application/json")
	cloudEvent.SetTime(now)

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			query := &storage.Query{Prefix: ""}
			it := actionsRepoBucket.Objects(ctx, query)
			for {
				attrs, err := it.Next()
				if err == iterator.Done {
					break
				}
				if err != nil {
					log.Fatalf("it.Next() %s", err)
				}
				err = actionsRepoBucket.Object(attrs.Name).Delete(ctx)
				if err != nil {
					log.Fatalf("actionsRepoBucket.Object(attrs.Name).Delete(ctx) %s", err)
				}
				t.Logf("deleted test data %s %s", actionsRepoBucketName, attrs.Name)
			}
			err := filepath.Walk(tc.folderPath, func(path string, info os.FileInfo, err error) error {
				if filepath.Ext(path) == ".json" {
					p := filepath.Clean(path)
					if !strings.HasPrefix(p, "testdata/") {
						panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
					}
					b, err := os.ReadFile(p)
					if err != nil {
						log.Fatal(err)
					}
					storageObject := actionsRepoBucket.Object(filepath.Base(path))
					storageObjectWriter := storageObject.NewWriter(context.Background())
					_, err = fmt.Fprint(storageObjectWriter, string(b))
					if err != nil {
						return err
					}
					err = storageObjectWriter.Close()
					if err != nil {
						return err
					}
					t.Logf("find %s, added %s to %s", path, storageObject.ObjectName(), storageObject.BucketName())
				}
				return nil
			})
			if err != nil {
				log.Fatal(err)
			}

			pubsubData := &cai.WrappedPubSub{}
			pubsubData.Message.ID = cloudEvent.ID()
			pubsubData.Subscription = "projects/qwerty-ram-dev-100/subscriptions/eventarc-europe-west1-convertfeed-sub-999"
			pubsubData.Message.Data = []byte(tc.trigger)

			cloudEvent.SetData("application/json", pubsubData)

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			err = EntryPoint(ctx, cloudEvent)
			if err != nil {
				t.Errorf("want no error and got %v", err)
			}
			msgString := buffer.String()
			// t.Logf("%v", msgString)
			// msgString := ""

			for _, wantMsg := range tc.wantMsgContains {
				wantMsg := wantMsg
				if !strings.Contains(msgString, wantMsg) {
					t.Errorf("want msg to contains: %s", tc.wantMsgContains)
				}
			}
			logEntriesString := strings.Split(msgString, "\n")
			for _, logEntryString := range logEntriesString {
				var logEntry glo.Entry
				err = json.Unmarshal([]byte(logEntryString), &logEntry)
				if !(err != nil) {
					if logEntry.MicroserviceName == "" {
						t.Errorf("logEntry.MicroserviceName is null should not %s", logEntry.Message)
					}
					if logEntry.Environment == "" {
						t.Errorf("logEntry.Environment is null should not %s", logEntry.Message)
					}
					if strings.HasPrefix(strings.ToLower(logEntry.Message), "finish") {
						if len(logEntry.StepStack) == 0 {
							t.Errorf("logEntry.StepStack len is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencyE2ESeconds == 0 {
							t.Errorf("logEntry.LatencyE2ESeconds is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencySeconds == 0 {
							t.Errorf("logEntry.LatencySeconds is zero and should not %s", logEntry.Message)
						}
					}
				}
			}
		})
	}
}
