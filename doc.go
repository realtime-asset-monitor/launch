// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*
Package launch in the launch go module is a function based on the function framework to launch scheduled actions

# Triggered by

# A cloud event containing the Cloud Scheduler message body used as a key to fetch which actions to launch

# Output

- actions to be launched, examples: CAI/export assets, GCI/list groups ...

# Cardinality

- one-many

# Automatic retrying

Yes.
*/
package launch
